#!/bin/env fennel
(local feed (require :feed))
(local hercules (require :hercules))
(local uuids (require :uuids))

;;; code:


(local feedprops
       (feed.props
        {
         :title "hello atom feed"
         :subtitle "hello atom feed"
         :link "https://erik.lundstedt.it/index.html"
         :location "https://erik.lundstedt.it/feed.atom"
         :updated {:year 2022 :month 06 :day 21}
         :id uuids.feeds.helloworld.uuid
         :author {:name "Erik Lundstedt" :email "erik@lundstedt.it"}
         }
        )
       )

(local entries
       {
        :test
        (feed.entry
         {
          :title "test entry"
          :subtitle "test entry"
         :link "https://erik.lundstedt.it"
         :summary ["this is just a test"]
         :published {:year 2022 :month 06 :day 21}
         :id uuids.feeds.helloworld.entrys.first.uuid
         :content ["here is some content" "and here is some more content"]
         })
        }
       )




(hercules.writeFile (feed.build feedprops [entries.test]) "atom.xml")
(hercules.preview (feed.build feedprops [entries.test]))
