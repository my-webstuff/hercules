#!/bin/env fennel
(local feed {})

(local hercules (require :hercules))



;;; commentary:
;;;; feed head
;; <?xml version="1.0" encoding="utf-8"?>
   ;; <feed xmlns="http://www.w3.org/2005/Atom">
;;;; feed properties
   ;;   <title>Example Feed</title>
   ;;   <link href="http://example.org/"/>
   ;;   <updated>2003-12-13T18:30:02Z</updated>
   ;;   <author>
   ;;     <name>John Doe</name>
   ;;   </author>
   ;;   <id>urn:uuid:60a76c80-d399-11d9-b93C-0003939e0af6</id>
;;;; example feed entry
   ;;   <entry>
   ;;     <title>Atom-Powered Robots Run Amok</title>
   ;;     <link href="http://example.org/2003/12/13/atom03"/>
   ;;     <id>urn:uuid:1225c695-cfb8-4ebb-aaaa-80da344efa6a</id>
   ;;     <updated>2003-12-13T18:30:02Z</updated>
   ;;     <summary>Some text.</summary>
   ;;   </entry>
   ;; </feed>
;;; code:
;;;; functions
(fn feed.title [title]
  "title of the feed or feed-entry"
  (.. "<title>" title "</title>")
  )

(fn feed.subtitle [title]
  "title of the feed or feed-entry"
  (.. "<subtitle>" title "</subtitle>")
  )

(fn feed.link [href rel]
  "just a link"
  (var ref "")
  (when (not= rel nil)
	  (set ref (.. "rel=\"" rel "\" " ))
	  )
  (.. "<link " ref "href=\"" href "\"/>")
  )

(fn datetime [date]
  (os.date "%FT%T" (os.time date))
  )

(fn feed.updated [date timezone]
  "remember to give {:year  :month  :day } and optionaly add  ... :hour :min :sec}"
  (.. "<updated>" (datetime date) (or timezone "+02:00") "</updated>" )
  )

(fn feed.published [date timezone]
  "remember to give {:year  :month  :day } and optionaly add  ... :hour :min :sec}"
  (.. "<published>" (datetime date) (or timezone "+02:00") "</published>" )
  )

  (fn feed.id [uuid]
	(table.concat
	 [
	  "<id>" uuid "</id>"
	  ]
	 )
	)
(fn feed.author [author]
  (fn nametag [name]
	(if (not= name nil)
		(.. "<name>" name "</name>\n")
		""
		)
	)
  (fn mailtag [addr]
	(if (not= addr nil)
		(.. "<email>" addr "</email>\n")
		""
		)
	)

  (fn uritag [uri]
	(if (not= uri nil)
		(.. "<uri>" uri "</uri>\n")
		""
		)
	)



  (table.concat
   ["<author>\n"
	(nametag author.name)
	(mailtag author.email)
	(uritag  author.uri)
	"\t</author>\n"
	]
   )
  )
(fn feed.summary [summary]
  (table.concat
   [
	"<summary>\n"
	(table.concat
	 summary "\n"
	 )
	"\n</summary>"
	]
   )
  )
(fn feed.content [content]
  (table.concat
   [
	"<content>\n"
	(table.concat
	 content "\n"
	 )
	"\n</content>"
	]
   )
  )





;;;; {{{}}}

(fn feed.entry [entry]
  (table.concat
   [
	"\t<entry>"
	(.. "\t" (feed.published entry.published))
	(.. "\t" (feed.updated (or entry.updated entry.published)))
	(.. "\t" (feed.id entry.id))
	(.. "\t" (feed.title entry.title))
	(.. "\t" (feed.subtitle (or entry.subtitle entry.title)))
	(.. "\t" (feed.link entry.link))
	(.. "\t" (feed.summary entry.summary))
	(.. "\t" (feed.content entry.content))
	(.. "\t" (feed.author entry.author))
	"</entry>"
	]
   "\n\t"
   )
  )



;;;; setup needed properties for the FEED
(fn feed.props [props]
  "setup needed properties for the FEED"
  (table.concat
   [
	(feed.title props.title)
	(feed.subtitle props.subtitle)
	(feed.link props.link)
	(feed.link props.location "self")
	(feed.id props.id)
	(feed.updated props.updated)
	(feed.author props.author)
	]
   "\n\t"
   )
  )


;;;; build the feed
(fn feed.build [props entrys]
"build the feed"
  (local header (table.concat
				  [
				   "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				   "<feed xmlns=\"http://www.w3.org/2005/Atom\">"
				   ]
				  "\n"))
  (table.concat
   [
	header
	(.. "\t" props)
	(table.concat entrys)
	"</feed>"
	]
   "\n"
   )
  )


feed
