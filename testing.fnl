#!/bin/env fennel
(local testing {})


;;; code:

(fn datetime [date]
  (print
   (table.concat
	[
	 (os.date "%FT%T%z" (os.time date))
	 ]
   )
   )
  )
(datetime {{:year 1999 :month 7 :day 6 :hour 3 :min 8 :sec 20}})


testing
