#!/usr/bin/env fennel

(local style {
			  :grid {}
			  })


(fn style.grid.template-columns [vals]
  (local retval
		 [
		  "grid-template-columns:"
		  ])
  (each [k v (ipairs vals)]
	(table.insert retval (.. " " (tostring v) "fr"))
	)
	(table.insert retval ";")
  (table.concat retval )
  )


(fn style.grid.template-rows [vals]
  (local retval
		 [
		  "grid-template-rows:"
		  ])
  (each [k v (ipairs vals)]
	(table.insert retval (.. " " (tostring v) "fr"))
	)
	(table.insert retval ";")
  (table.concat retval )
  )

(fn style.grid.gap [vals]
  (local retval
		 [
		  "gap:"
		  ])
  (each [k v (ipairs vals)]
	(table.insert retval (.. " " (tostring v) "px"))
	)
	(table.insert retval ";")
  (table.concat retval )
  )


(fn style.repeat [times v]
  (local retval ["" "" ""])
  (for [i 1 times]
	(tset retval i v)
	)
  retval
  )

 (fn style.grid.template-areas [areas]
   (var retval ["grid-template-areas:"])
   (each [i v (ipairs areas)]
	 (table.insert retval (.. "\t\t\"" v "\""))
	 )
   (set retval (.. (table.concat retval "\n") ";"))
   retval
   )
(fn style.grid.area [name]
  (table.concat ["." name "{" "grid-area: " name "; }"])
  )

(local grid style.grid)


(local template-areas
	   [
		(table.concat (style.repeat 3 "pagehead") " ")
		(table.concat ["commonLinks" "xplr" "internal"] " ")
		(table.concat ["commonLinks" "suckless" "internal"] " ")
		(table.concat ["pagehead" "pageHead" "info"] " " )
		])



(print
 (table.concat
  [
   ".container {"
   "display: grid;"
   (grid.template-columns [1 1 1])
   (grid.template-rows [0.5 1 1 0.5])
   (grid.gap [0 0])
   (grid.template-areas template-areas)
   "}"
   ]
  "\n\t"
  )
 )

(local areas
	   {
		:pagehead "pagehead"
		:commonLinks "commonLinks"
		:internal  "internal"
		:xplr "xplr"
		:suckless "suckless"
		:awesomewm "awesomewm"
		:info "info"
		}
	   )
(each [k v (pairs areas)]
  (print (grid.area v) )
  )



style
