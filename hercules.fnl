#!/usr/bin/env fennel


(local hercules {})


(fn hercules.preview [content]
  (print content)
)


(fn hercules.writeFile [content filename]
  (with-open [fout (io.open filename :w) ]
    (fout:write content)
    ) ; => first line of index.html
  (print (.. "wrote " "data " "to " filename))
  )







hercules
